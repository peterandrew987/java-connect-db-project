package com.peter;

import org.junit.Test;

import static org.junit.Assert.*;

public class ConnectorTest {

    @Test
    public void connectToDB() {
        Connector connector = new Connector();
        assertEquals("connection successfull",connector.connectToDB("localhost","root",""));
    }

    @Test
    public void addDataTest(){
        Connector connector = new Connector();
        connector.connectToDB("localhost/jdbc","root","");
        String message = connector.addRecord("peter","082273774781","09090990","medan","90St.Prokey","800000");
        assertEquals("add data success",message);
    }

    @Test
    public void updateDataTest(){
        Connector connector = new Connector();
        connector.connectToDB("localhost/jdbc","root","");
        String message = connector.updateData("peter","00000","25323","jakarta","St.Prokey:70th","1800000");
        assertEquals("update success",message);
    }

    @Test
    public void deleteDataTest(){
        Connector connector = new Connector();
        connector.connectToDB("localhost/jdbc","root","");
        String message = connector.deleteData("peter");
        assertEquals("delete success",message);
    }
}
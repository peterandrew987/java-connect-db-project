package com.peter;

import java.sql.*;
import java.util.ArrayList;

public class Connector implements DBConnection {
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    @Override
    public String connectToDB(String url,String user,String password) {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:mysql://"+url,
                    user,
                    password
            );
            return "connection successfull";
        }catch (Exception e)
        {
            return e.getMessage();
        }
    }

    @Override
    public void closeDB() {
        try{
            preparedStatement.close();
            connection.close();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public String addRecord(String name,String mobileno,String idcardno,String city,String address,String salary) {
        String sql = "INSERT INTO employee(name,mobileno,idcardno,city,address,salary) VALUES ('"+name+"','"+mobileno+"','"+idcardno+"','"+city+"','"+address+"','"+salary+"')";
        try {
            preparedStatement = connection.prepareStatement(sql);
            System.out.println(preparedStatement);
            System.out.println("Data Recorded");
            preparedStatement.execute();
        } catch (Exception e){
            return e.getMessage();
        }
        finally {
           this.closeDB();
        }
        return "add data success";
    }
    public ArrayList<Employee> getData(){
        ResultSet data = null;
        Employee employee = null;
        ArrayList<Employee> employeelist = new ArrayList<>();
        String sql = "SELECT * FROM employee";
        try{
            preparedStatement = connection.prepareStatement(sql);
            data = preparedStatement.executeQuery();
            do {
                data.next();
                employee = new Employee(data.getString("name"), data.getString("mobileno"), data.getString("idcardno"), data.getString("city"), data.getString("address"), data.getString("salary"));
                employeelist.add(employee);
            } while(data.isLast()!=true);
        } catch (Exception e) {
            System.out.println("Connection time out \nBecause :"+e.getMessage());
        } finally {
            this.closeDB();
        }
        return employeelist;
    }

    public String updateData(String name,String mobileno,String idcardno,String city,String address,String salary){
        String sql = "UPDATE employee SET mobileno='"+mobileno+"',idcardno='"+idcardno+"',city='"+city+
                "',address='"+address+"',salary='"+salary+
                "' WHERE name='"+name+"'";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();
            System.out.println("Update Succesfull");
        } catch (Exception e) {
            return e.getMessage();
        } finally {
            this.closeDB();
        }
        return "update success";
    }
    public String deleteData(String name){
        String sql = "DELETE FROM employee WHERE name='"+name+"'";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            return e.getMessage();
        } finally {
            closeDB();
        }
        return "delete success";
    }
}

package com.peter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface DBConnection {
    public String connectToDB(String url,String user,String pass);
    public void closeDB() throws SQLException;
}

package com.peter;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static Connector connector = new Connector();
    public static void main(String[] args) {
        registerEmployee();
    }
    public static void registerEmployee(){
        String name = "",mobileno = "",city = "",address = "";
        String salary = "",idcardno = "";
        Scanner sc;
        sc = new Scanner(System.in);
        System.out.println("====================================");
        System.out.println("please Enter your name :");
        name = sc.nextLine();
        System.out.println("please Enter your mobile number :");
        mobileno = sc.nextLine();
        System.out.println("please Enter your id card number :");
        idcardno = sc.nextLine();
        System.out.println("please Enter your city :");
        city = sc.nextLine();
        System.out.println("please Enter your address :");
        address = sc.nextLine();
        System.out.println("please Enter your salary :");
        salary = sc.nextLine();
        System.out.println("====================================");
        connector.connectToDB("localhost/jdbc","root","");
        connector.addRecord(name,mobileno,idcardno,city,address,salary);
        System.out.println("Thanks for using our service");
        System.out.println("====================================");
    }
    public static void getEmployee(){
        ArrayList<Employee> employeeList = connector.getData();
        for(Employee employee : employeeList){
            System.out.println("=======================");
            System.out.println(employee.name);
            System.out.println(employee.mobileno);
            System.out.println(employee.idcardno);
            System.out.println(employee.city);
            System.out.println(employee.address);
            System.out.println(employee.salary);
        }
        System.out.println("........");


    }
}
